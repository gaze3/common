package filter

// BasicFilter ...
type BasicFilter struct {
	where   Where
	preload Preload
	joins   Joins
	groups  Groups
	keys    map[string]bool
}

// NewBasicFilter ...
func NewBasicFilter() *BasicFilter {
	return &BasicFilter{
		where:   Where{},
		preload: Preload{},
		joins:   Joins{},
		groups:  Groups{},
		keys:    map[string]bool{},
	}
}

// GetLimit implement repository.Filter interface
func (f *BasicFilter) GetLimit() int {
	return IgnoreLimit
}

// GetOffset implement repository.Filter interface
func (f *BasicFilter) GetOffset() int {
	return IgnoreOffset
}

// GetWhere implement repository.Filter interface
func (f *BasicFilter) GetWhere() Where {
	return f.where
}

// GetPreload implement repository.Filter interface
func (f *BasicFilter) GetPreload() Preload {
	return f.preload
}

// GetJoins implement repository.Filter interface
func (f *BasicFilter) GetJoins() Joins {
	return f.joins
}

// GetGroupBy implement repository.Filter interface
func (f *BasicFilter) GetGroupBy() Groups {
	return f.groups
}

// AddWhere implement repository.Filter interface
func (f *BasicFilter) AddWhere(key string, query string, values ...interface{}) *BasicFilter {
	f.where[query] = values
	f.keys[key] = true
	return f
}

// AddPreload implement repository.Filter interface
func (f *BasicFilter) AddPreload(key string, values ...interface{}) *BasicFilter {
	f.preload[key] = values
	return f
}

// AddJoin implement repository.Filter interface
func (f *BasicFilter) AddJoin(key string, query ...string) *BasicFilter {
	f.joins[key] = query
	return f
}

// AddGroup implement repository.Filter interface
func (f *BasicFilter) AddGroup(group string) *BasicFilter {
	f.groups = append(f.groups, group)
	return f
}
