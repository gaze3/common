package user_group_permission

import (
	userModuleModel "gitlab.com/gaze3/common/model/user_module"
	userPermissionModel "gitlab.com/gaze3/common/model/user_permission"
)

type UserGroupPermission struct {
	UserPermission *userPermissionModel.UserPermission `json:"userPermission,omitempty"`
	UserModule     *userModuleModel.UserModule         `json:"userModule,omitempty"`
}
