package constant

type EmploymentType string

const (
	Daily       EmploymentType = "DAILY"
	Weekly      EmploymentType = "WEEKLY"
	SemiMonthly EmploymentType = "SEMI_MONTHLY"
	Monthly     EmploymentType = "MONTHLY"
)

var EmploymentTypes = map[string]EmploymentType{
	"DAILY":        Daily,
	"WEEKLY":       Weekly,
	"SEMI_MONTHLY": SemiMonthly,
	"MONTHLY":      Monthly,
}
