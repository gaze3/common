package config

import (
	"gitlab.com/gaze3/common/constant"

	"github.com/kelseyhightower/envconfig"
)

type AppConfig struct {
	HTTPHost         string           `envconfig:"HTTP_HOST" required:"true"`
	HTTPPort         string           `envconfig:"HTTP_PORT" required:"true"`
	FileUploaderHost string           `envconfig:"FILE_UPLOADER_HOST" required:"true"`
	APIKey           string           `envconfig:"API_KEY" required:"true"`
	GinMode          constant.GinMode `envconfig:"GIN_MODE" required:"true"`
	AppMode          constant.AppMode `envconfig:"APP_MODE" required:"true"`
}

func ProvideAppConfig() *AppConfig {
	var conf AppConfig
	envconfig.MustProcess("", &conf)
	return &conf
}
